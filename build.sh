#!/usr/bin/env bash

set -eu

DOCKER_REPO='ezalejski'
DOCKER_NAME='nginx_error_test'
DOCKER_TAG=${1:-"latest"}
DOCKER_IMAGE="$DOCKER_REPO/$DOCKER_NAME"

DOCKER=$(command -v docker) || { echo "docker is not available!"; exit 1; }

set -x
${DOCKER} build -t "${DOCKER_IMAGE}:${DOCKER_TAG}" .
